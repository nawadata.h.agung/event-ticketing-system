# KPOP Concert Ticketing System

## Overview
A simple system for customer to buy ticket of K-POP concert.

## Requirements
- User roles are admin and member
- User role will contains many permissions
- Admin user can manage the concert data
- Admin user can manage the merchandises of the concert
- User can buy a ticket without login first
- State of event such as Available, Full Booked, Sold Out, Closed, and Cancelled
- State of ticket order such as Waiting List, Booked, Paid, and Cancelled

## Stack Technologies
- ASP.NET Core MVC
- ASP.NET Core Web API
- Dapper.NET
- SQL Server
- Evolve Database Migration

## Installations
1. Git clone this project
2. Run database migration
   - Download Evolve CLI at https://github.com/lecaillon/Evolve/releases
   - Add the evolve.exe in PATH variable at system environment
   - Go to project directory EventTicketing.WebAPI.DbMigrations
   - Open command line and run: evolve.exe migrate config.txt
3. Run Application