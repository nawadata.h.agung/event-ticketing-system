﻿using EventTicketing.WebAPI.UserManagement.Requests;
using EventTicketing.WebAPI.UserManagement.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace EventTicketing.WebAPI.Controllers
{
    [ApiController]
    public class GetUserListController : ControllerBase
    {
        private readonly GetUserListService _service;

        public GetUserListController(GetUserListService service)
        {
            _service = service;
        }

        [Route("api/v1/users")]
        public async Task<IActionResult> HandleAsync([FromQuery]GetUserListRequest requestModel)
        {
            if (requestModel == null)
            {
                throw new ArgumentNullException(nameof(requestModel));
            }

            var data = await _service.HandleAsync(requestModel);
            return Ok(data);
        }
    }
}
