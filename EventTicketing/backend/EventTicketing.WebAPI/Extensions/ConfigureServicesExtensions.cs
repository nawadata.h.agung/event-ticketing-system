﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;

namespace EventTicketing.WebAPI.Extensions
{
    public static class ConfigureServicesExtensions
    {
        public static void ConfigureAppServices(this IServiceCollection services)
        {
            services.AddSingleton<Shared.DbContext>();

            services.AddScoped<UserManagement.QueryRepositories.UserRepository>();
            services.AddScoped<UserManagement.Filters.GetUserListPagingFilterBuilder>();
            services.AddScoped<UserManagement.Services.GetUserListService>();
        }

        public static void ConfigureObjectMappings(this IServiceCollection services)
        {
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new ObjectMappings.UserManagementProfile());
            });

            IMapper mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);
        }
    }
}
