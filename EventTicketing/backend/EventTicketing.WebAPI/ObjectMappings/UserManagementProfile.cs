﻿using AutoMapper;
using EventTicketing.WebAPI.UserManagement.Models;
using EventTicketing.WebAPI.UserManagement.Responses;

namespace EventTicketing.WebAPI.ObjectMappings
{
    public class UserManagementProfile : Profile
    {
        public UserManagementProfile()
        {
            CreateMap<UserModel, UserListResponse>();
        }
    }
}
