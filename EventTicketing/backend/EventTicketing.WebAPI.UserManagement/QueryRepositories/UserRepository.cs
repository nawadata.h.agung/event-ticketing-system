﻿using Dapper;
using EventTicketing.WebAPI.Shared;
using EventTicketing.WebAPI.UserManagement.Models;
using EventTicketing.WebAPI.UserManagement.SqlParams;
using EventTicketing.WebAPI.UserManagement.SqlQueries;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EventTicketing.WebAPI.UserManagement.QueryRepositories
{
    public class UserRepository
    {
        private readonly DbContext _dbContext;

        public UserRepository(DbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IEnumerable<UserModel>> GetUserListAsync(UserPagingParams sqlParams)
        {
            using (var connection = _dbContext.CreateConnection())
            {
                connection.Open();

                string sql = UserQueries.GetUserList + " ";
                sql += string.IsNullOrWhiteSpace(sqlParams.FilterClause) ? "" : sqlParams.FilterClause;

                return await connection.QueryAsync<UserModel>(sql, sqlParams);
            }
        }
    }
}
