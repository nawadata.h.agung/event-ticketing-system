﻿using EventTicketing.WebAPI.Shared;

namespace EventTicketing.WebAPI.UserManagement.Requests
{
    public class GetUserListRequest : BasePagingRequest
    {
        public string ConcertName { get; set; }
        public string ConcertPlace { get; set; }
        public string ConcertDate { get; set; }
    }
}
