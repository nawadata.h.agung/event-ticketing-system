﻿using EventTicketing.WebAPI.UserManagement.Requests;
using EventTicketing.WebAPI.UserManagement.SqlParams;

namespace EventTicketing.WebAPI.UserManagement.Filters
{
    public class GetUserListPagingFilterBuilder
    {
        public GetUserListPagingFilterBuilder()
        {
        }

        public UserPagingParams Build(GetUserListRequest request)
        {
            var sqlParams = new UserPagingParams
            {
                FilterClause = "WHERE 1=1 ",
                Start = request.Start,
                Length = request.Length
            };

            if (string.IsNullOrWhiteSpace(request.OrderBy))
            {
                sqlParams.OrderBy = "ConcertTime DESC";
            }

            if (!string.IsNullOrEmpty(request.ConcertName))
            {
                sqlParams.FilterClause += "AND ConcertName LIKE @ConcertName";
                sqlParams.ConcertName = $"%{request.ConcertName}%";
            }

            if (!string.IsNullOrEmpty(request.ConcertPlace))
            {
                sqlParams.FilterClause += "AND ConcertPlace LIKE @ConcertPlace";
                sqlParams.ConcertPlace = $"%{request.ConcertPlace}%";
            }

            if (!string.IsNullOrEmpty(request.ConcertDate))
            {
                sqlParams.FilterClause += "AND ConcertDate LIKE @ConcertDate";
                sqlParams.ConcertDate = $"%{request.ConcertDate}%";
            }

            return sqlParams;
        }
    }
}
