﻿using AutoMapper;
using EventTicketing.WebAPI.Shared;
using EventTicketing.WebAPI.UserManagement.Filters;
using EventTicketing.WebAPI.UserManagement.QueryRepositories;
using EventTicketing.WebAPI.UserManagement.Requests;
using EventTicketing.WebAPI.UserManagement.Responses;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EventTicketing.WebAPI.UserManagement.Services
{
    public class GetUserListService
    {
        private readonly UserRepository _userRepository;
        private readonly GetUserListPagingFilterBuilder _filterBuilder;
        private readonly IMapper _mapper;

        public GetUserListService(
            UserRepository userRepository,
            GetUserListPagingFilterBuilder filterBuilder,
            IMapper mapper)
        {
            _userRepository = userRepository;
            _filterBuilder = filterBuilder;
            _mapper = mapper;
        }

        public async Task<BasePagingResponse<UserListResponse>> HandleAsync(GetUserListRequest request)
        {
            var sqlParams = _filterBuilder.Build(request);
            var data = await _userRepository.GetUserListAsync(sqlParams);
            var mappedData = _mapper.Map<IEnumerable<Models.UserModel>, IEnumerable<UserListResponse>>(data);

            return new BasePagingResponse<UserListResponse>
            {
                Data = mappedData.ToList(),
                TotalRecord = mappedData.Count()
            };
        }
    }
}
