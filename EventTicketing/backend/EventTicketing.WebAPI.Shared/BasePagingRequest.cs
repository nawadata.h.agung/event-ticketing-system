﻿namespace EventTicketing.WebAPI.Shared
{
    public class BasePagingRequest
    {
        public int Start { get; set; }
        public int Length { get; set; }
        public string OrderBy { get; set; }
    }
}
