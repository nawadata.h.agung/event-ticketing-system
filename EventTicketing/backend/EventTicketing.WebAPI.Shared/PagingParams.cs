﻿using System;

namespace EventTicketing.WebAPI.Shared
{
    public class PagingParams
    {
        public int Start { get; set; }
        public int Length { get; set; }
        public string FilterClause { get; set; }
        public string OrderBy { get; set; }

        public virtual void Validate()
        {
            if (string.IsNullOrWhiteSpace(OrderBy)) throw new ArgumentException("PagingParams.OrderBy value is null or empty.");
        }

    }
}
