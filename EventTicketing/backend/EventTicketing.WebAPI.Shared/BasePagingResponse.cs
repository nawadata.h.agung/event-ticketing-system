﻿using System.Collections.Generic;

namespace EventTicketing.WebAPI.Shared
{
    public class BasePagingResponse<T> where T: class
    {
        public List<T> Data { get; set; }
        public int TotalRecord { get; set; }
    }
}
