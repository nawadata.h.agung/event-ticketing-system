DROP TABLE IF EXISTS dbo.Permissions
GO

CREATE TABLE dbo.Permissions
(
	PemissionId BIGINT PRIMARY KEY IDENTITY (1, 1),
    PermissionName VARCHAR (20) NOT NULL,
	PermissionDescription VARCHAR (500) NULL,
	CreatedBy VARCHAR (20) NOT NULL,
	CreatedDate DATETIME NOT NULL,
	UpdatedBy VARCHAR (20) NULL,
	UpdatedDate DATETIME NULL
)
GO
