DROP TABLE IF EXISTS dbo.ConcertStages
GO

CREATE TABLE dbo.ConcertStages
(
	ConcertStageId BIGINT PRIMARY KEY IDENTITY (1, 1),
	ConcertId BIGINT NOT NULL,
    StageName VARCHAR (20) NOT NULL,
	MaxCapacity VARCHAR (100) NOT NULL,
    Price DECIMAL(18,2) NOT NULL,
	IsActive BIT NOT NULL,
	CreatedBy VARCHAR (20) NOT NULL,
	CreatedDate DATETIME NOT NULL,
	UpdatedBy VARCHAR (20) NULL,
	UpdatedDate DATETIME NULL,
    FOREIGN KEY (ConcertId) REFERENCES dbo.Concerts (ConcertId)
)
GO
