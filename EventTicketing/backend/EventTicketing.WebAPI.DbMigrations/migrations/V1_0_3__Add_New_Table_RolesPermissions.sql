DROP TABLE IF EXISTS dbo.RolePermissions
GO

CREATE TABLE dbo.RolePermissions
(
	RolePermissionId BIGINT PRIMARY KEY IDENTITY (1, 1),
    RoleId BIGINT NOT NULL,
	PermissionId BIGINT NOT NULL,
	CreatedBy VARCHAR (20) NOT NULL,
	CreatedDate DATETIME NOT NULL,
    FOREIGN KEY (RoleId) REFERENCES dbo.Roles (RoleId),
	FOREIGN KEY (PermissionId) REFERENCES dbo.Permissions (PermissionId)
)
GO
