DROP TABLE IF EXISTS dbo.Concerts
GO

CREATE TABLE dbo.Users
(
	ConcertId BIGINT PRIMARY KEY IDENTITY (1, 1),
    ConcertName VARCHAR (100) NOT NULL,
	ConcertDetail VARCHAR (MAX) NOT NULL,
	ConcertPlace DATETIME NOT NULL,
    ConcertTime DATETIME NOT NULL,
	Status VARCHAR (20) NOT NULL,
	MaxCapacity INT NOT NULL,
	MaxWaitingList INT NOT NULL,
	EnableMerchandiseDiscount BIT NOT NULL,
	MerchandiseDiscount DECIMAL (18,2) NOT NULL,
	IsActive BIT NOT NULL,
	CreatedBy VARCHAR (20) NOT NULL,
	CreatedDate DATETIME NOT NULL,
	UpdatedBy VARCHAR (20) NULL,
	UpdatedDate DATETIME NULL
)
GO
